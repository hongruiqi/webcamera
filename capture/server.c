/* A simple server in the internet domain using TCP
   The port number is passed as an argument
   This version runs forever, forking off a separate
   process for each connection
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define BUFFER_INIT_SIZE 8192
#define BUFFER_INC_SIZE 8192

void dostuff(int); /* function prototype */
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, pid;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0)
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0)
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     while (1) {
         newsockfd = accept(sockfd,
               (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0)
             error("ERROR on accept");
         pid = fork();
         if (pid < 0)
             error("ERROR on fork");
         if (pid == 0)  {
             close(sockfd);
             dostuff(newsockfd);
             exit(0);
         }
         else {
                close(newsockfd);
                exit(0);
         }
     } /* end of while */
     close(sockfd);
     return 0; /* we never get here */
}

/******** DOSTUFF() *********************
 There is a separate instance of this function
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
void dostuff (int sock)
{
    int n;
    int n_read = 0;
    uint8_t *buffer;
    int size = BUFFER_INIT_SIZE;
    int frame_size = 0;

    FILE *f = fopen("test.x264", "a");
    while(1) {
        n = read(sock, &frame_size, 4);
        printf("nnnnnnnnnnnnnn");
        if (n < 0) {
            printf("frame size");
            break;
        }
        // if (n < 0) error("ERROR reading frame size from socket");
        buffer = (uint8_t *)malloc(frame_size * sizeof(uint8_t));
        while (n_read < frame_size) {
            n = read(sock, buffer + n_read, frame_size - n_read);
            if (n < 0) {
                printf("frame data");
                break;
            }
            // if (n < 0) error("ERROR reading frame data from socket");
            n_read += n;
        }
        int ret = fwrite(buffer, 1, frame_size, f);
        printf("%d\n\n", ret);
    }
    printf("here!!!!!");
    fclose(f);
}

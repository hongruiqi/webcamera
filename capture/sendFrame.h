#ifndef SENDFRAME_H_INCLUDED
#define SENDFRAME_H_INCLUDED

#include <stdint.h>
#include "x264Encoder.h"

int socket_open(char *domain, int portno);
int send_frame(int sockfd, x264_nal_t *nals, int i_nals);
int socket_send(int sockfd, uint8_t *buffer, int length);
int socket_close(int sockfd);

#endif // SENDFRAME_H_INCLUDED

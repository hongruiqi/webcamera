#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <linux/videodev2.h>
#include "x264Encoder.h"
#include "capture.h"
#include "sendFrame.h"

#define FPS 25

int main() {
    /* 初始化 Capture */
    Capture *capture = malloc(sizeof(Capture));
    capture->dev = "/dev/video0";
    capture->width = 640;
    capture->height = 400;
    capture->pixelformat = V4L2_PIX_FMT_YUYV;
    int ret = capture_open(capture);
    if(ret < 0) {
        fprintf(stderr, "Capture fail, exit.\n");
        return -1;
    }
    /* 初始化 x264Encoder */
    x264Encoder enc;
    enc.fps = FPS;
    enc.height = 400;
    enc.width = 640;
    x264Encoder_open(&enc);
    /* 打开摄像头 */
    ret = capture_start(capture);
    if(ret < 0) {
        fprintf(stderr, "Capture fail, exit.\n");
        return -1;
    }
    char *domain = "localhost";
    int port = 70;
    int sockfd;
    sockfd = socket_open(domain, port);
    if (sockfd < 0) {
        fprintf(stderr, "Fail to open socket.\n");
        return -1;
    }
    uint8_t *pic;
    int i = 0;
    for(i = 0; i < 50; i++) {
        ret = capture_shot(capture, &pic);
        if(ret < 0) {
            fprintf(stderr, "capture_shot error");
            return -1;
        }
        x264_nal_t *nals;
        ret = x264Encoder_encode(&enc, (uint8_t *)pic, capture->width, capture->height, &nals);
        printf("buffer_used: %d\n", enc.buffer_used);
        if (ret < 0) {
            fprintf(stderr, "x264Encoder_encode error");
            return -1;
        }
        if (send_frame(sockfd, nals, ret) < 0) {
            socket_close(sockfd);
        }
        usleep(1000 / FPS);
        free(pic);
    }
    socket_close(sockfd);

    x264Encoder_close(&enc);
    ret = capture_close(capture);
    if(ret < 0) {
        fprintf(stderr, "Capture fail, exit.\n");
        return -1;
    }
    free(capture);
    return 0;
}

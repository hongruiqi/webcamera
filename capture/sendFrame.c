#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "sendFrame.h"
#include "x264Encoder.h"

int socket_open(char *domain, int portno) {
    int sockfd;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("ERROR opening socket");
        goto ERROR;
    }
    server = gethostbyname(domain);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        return -1;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        goto ERROR;
    }
ERROR:
    return sockfd;
}

int send_frame(int sockfd, x264_nal_t *nals, int i_nals) {
    int i, n;
    int sequence_length = 6;
    int header_status = 0;
    uint8_t *buffer, *sequence_header;
    if (header_status == 0) {
        sequence_header = (uint8_t *)malloc(sequence_length);
        sequence_header[0] = 0x01;
        sequence_header[4] = 0xff;
        sequence_header[5] = 0xe1;
    }
    for (i = 0; i < i_nals; i++) {
//        int j;
//        fprintf(stderr, "length: %x; ", nals[i].i_payload);
//        for (j = 0; j < nals[i].i_payload; j++) {
//            fprintf(stderr, "%x ", nals[i].p_payload[j]);
//        }
//        fprintf(stderr, "\n");
        if (nals[i].i_type == 0) {
            continue;
        }
        if ((nals[i].i_type == 7) && (header_status == 0)) {
            sequence_header[1] = 0x42;//nals[i].p_payload[5];
            sequence_header[2] = 0xc0;//nals[i].p_payload[6];
            sequence_header[3] = 0x1e;//nals[i].p_payload[7];
            sequence_length = sequence_length + nals[i].i_payload - 2;
            sequence_header = realloc(sequence_header, sequence_length);
            uint16_t sps_length = (uint16_t)(nals[i].i_payload - 4);
            uint16_t sps_length_le = (sps_length >> 8) | (sps_length << 8);
            memcpy(&sequence_header[6], &sps_length_le, sizeof(uint16_t));
            memcpy(&sequence_header[8], &(nals[i].p_payload[4]), nals[i].i_payload - 4);
            header_status = 1;
            continue;
        }
        if ((nals[i].i_type == 8) && (header_status == 1)) {
            sequence_header = realloc(sequence_header, sequence_length + nals[i].i_payload - 1);
            sequence_header[sequence_length] = 0x01;
            uint16_t pps_length = (uint16_t)(nals[i].i_payload - 4);
            uint16_t pps_length_le = (pps_length >> 8) | (pps_length << 8);
            memcpy(&sequence_header[sequence_length + 1], &pps_length_le, sizeof(uint16_t));
            memcpy(&sequence_header[sequence_length + 3], &(nals[i].p_payload[4]), nals[i].i_payload - 4);
            sequence_length = sequence_length + nals[i].i_payload - 1;
            header_status = 2;
            continue;
        }
        if (header_status == 2) {
            sequence_length += 9;
            buffer = (uint8_t *)malloc(sequence_length);
            memcpy(buffer, &sequence_length, 4);
            buffer[4] = 0x17;
            buffer[5] = 0x00;
            buffer[6] = 0x00;
            buffer[7] = 0x00;
            buffer[8] = 0x00;
            memcpy(buffer + 9, sequence_header, sequence_length - 9);
            free(sequence_header);
            if ((n = socket_send(sockfd, buffer, sequence_length)) > 0) {
                header_status = 3;
                continue;
            } else {
                return n;
            }
        }
        if (header_status == 3) {
            sequence_length = nals[i].i_payload + 9;
            buffer = (uint8_t *)malloc(sequence_length);
            memcpy(buffer, &sequence_length, 4);
            uint16_t ref_idc = (uint16_t)htonl(nals[i].i_ref_idc);
            buffer[4] = 0x07 | (ref_idc << 4);
            buffer[5] = 0x01;
            buffer[6] = 0x00;
            buffer[7] = 0x00;
            buffer[8] = 0x00;
            memcpy(buffer + 9, nals[i].p_payload, sequence_length - 9);
            if ((n = socket_send(sockfd, buffer, sequence_length)) < 0) {
                return n;
            }
        }
    }
    return 0;
}

int socket_send(int sockfd, uint8_t *buffer, int length) {
    int n;
    n = write(sockfd, buffer, length);
    fprintf(stderr,"%d %d--------------------", length, n);
    if (n < 0) {
        perror("ERROR writing to socket");
        fprintf(stderr, "\n");
        return -1;
    }
    free(buffer);
    return n;
}

int socket_close(int sockfd) {
    close(sockfd);
    return sockfd;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "x264Encoder.h"
#define BUFFER_INIT_SIZE 8192
#define BUFFER_INC_SIZE 8192

/* yuyv to yuv */
void yuyv2yuv(uint8_t *pic, int width, int height, uint8_t **y, uint8_t **u, uint8_t **v) {
    uint8_t *y0, *u0, *v0;
    const int n = width * height * 8;
    y0 = malloc(n);
    u0 = malloc(n / 4);
    v0 = malloc(n / 4);
    *y = y0;
    *u = u0;
    *v = v0;
    int i, j;
    for(i = 0; i < width * height; i++)
        *y0++ = *(pic + i * 2);
    for(i = 0; i < height; i += 2) {
        for(j = 0; j < width * 2; j += 4) {
            *u0++ = *(pic + j + 1);
            *v0++ = *(pic + j + 3);
        }
        pic += width * 4;
    }
}

/* 编码器初始化 */
void x264Encoder_open(x264Encoder *enc) {
    /* 设置编码参数 */
    x264_param_t param;
    x264_param_default_preset(&param, "fast", "zerolatency");
    param.i_threads = 1;                  /* encode multiple frames in parallel */
    param.i_width = enc->width;
    param.i_height = enc->height;
    param.i_fps_num = enc->fps;
    param.i_fps_den = 1;
    param.i_keyint_max = enc->fps;        /* Force an IDR keyframe at this interval */
    // param.b_intra_refresh = 1;            /* Whether or not to use periodic intra refresh instead of IDR frames. */
    param.rc.i_rc_method = X264_RC_CRF;   /* X264_RC_* */
    param.rc.f_rf_constant = 25;          /* 1pass VBR, nominal QP */
    param.rc.f_rf_constant_max = 35;      /* In CRF mode, maximum CRF as caused by VBV */
    param.b_repeat_headers = 1;           /* put SPS/PPS before each keyframe */
    param.b_annexb = 1;                   /* if set, place start codes (4 bytes) before NAL units,
                                           * otherwise place size (4 bytes) before NAL units. */
    param.i_log_level = X264_LOG_DEBUG;
    x264_param_apply_profile(&param, "baseline");
    // param.i_bframe = 2;
    enc->encoder = x264_encoder_open(&param);
    /* 分配缓冲buffer */
    enc->buffer = malloc(BUFFER_INIT_SIZE);
    enc->buffer_size = BUFFER_INIT_SIZE;
    enc->buffer_used = 0;
    enc->pts = 0;
}

/* 编码器编码 */
int x264Encoder_encode(x264Encoder *enc, uint8_t *pic, int width, int height, x264_nal_t **nals) {
    x264_picture_t pic_in, pic_out;
    x264_picture_alloc(&pic_in, X264_CSP_I420, enc->width, enc->height);  // 初始化 picture 变量
    uint8_t *y, *u, *v;
    /* 分离 yuv */
    yuyv2yuv(pic, width, height, &y, &u, &v);
    pic_in.img.plane[0] = y;
    pic_in.img.plane[1] = u;
    pic_in.img.plane[2] = v;
    pic_in.img.plane[3] = NULL;
    /* 设置帧序列号 */
    pic_in.i_pts = enc->pts;
    enc->pts++;
    // x264_nal_t *nals;
    int i_nals;
    /* 编码 */
    int ret = x264_encoder_encode(enc->encoder, nals, &i_nals, &pic_in, &pic_out);
    if(ret < 0) {
        fprintf(stderr, "x264_encoder_encode error");
        return -1;
    }
    // x264_picture_clean(&pic_in);
    /* 释放 yuv 空间*/
    free(y);
    free(u);
    free(v);
//    int i;
    /* 编码输出拷贝到编码器缓冲区 */
//    for(i = 0; i < i_nals; i++) {
//        if(enc->buffer_used + nals[i].i_payload > enc->buffer_size) {
//            enc->buffer_size += nals[i].i_payload + BUFFER_INC_SIZE;
//            enc->buffer = realloc(enc->buffer, enc->buffer_size);
//        }
//        memcpy(enc->buffer + enc->buffer_used, nals[i].p_payload, nals[i].i_payload);
//        enc->buffer_used += nals[i].i_payload;
//    }
    return i_nals;
}

/* 编码器关闭 */
void x264Encoder_close(x264Encoder *enc) {
    x264_encoder_close(enc->encoder);
    free(enc->buffer);
}

// TODO: Capture Delay
// TODO: pic_in close

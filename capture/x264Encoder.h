#ifndef X264ENCODER_H_INCLUDED
#define X264ENCODER_H_INCLUDED
#include <stdint.h>
#include <sys/time.h>
#include <x264.h>

typedef struct x264Encoder {
    x264_t *encoder;
    int width;
    int height;
    int fps;
    uint8_t *buffer;
    int buffer_size;
    int buffer_used;
    int64_t pts;
} x264Encoder;

void x264Encoder_open(x264Encoder *enc);
int x264Encoder_encode(x264Encoder *enc, uint8_t *pic, int width, int height, x264_nal_t **nals);
void x264Encoder_close(x264Encoder *enc);

#endif // X264ENCODER_H_INCLUDED

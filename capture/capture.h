#ifndef CAPTURE_H_INCLUDED
#define CAPTURE_H_INCLUDED

#include <stdint.h>

typedef struct Capture {
    int fd;
    char *dev;
    int width;
    int height;
    int pixelformat;
    int bytesperline;
    int sizeimage;
    struct {
        void *start;
        size_t length;
    } *buffers;
    int nbuffers;
} Capture, *PCapture;

int capture_open(Capture *capture);
int capture_start(Capture *capture);
int capture_shot(Capture *capture, uint8_t **pic);
int capture_stop(Capture *capture);
int capture_close(Capture *capture);

#endif // CAPTURE_H_INCLUDED
